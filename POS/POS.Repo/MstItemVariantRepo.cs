﻿using System.Data.Entity;
using POS.Model;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repo
{
    public class MstItemVariantRepo
    {
        public static List<MstItemVariantViewModel> Get()
        {
            List<MstItemVariantViewModel> result = new List<MstItemVariantViewModel>();
            using (POSContext context = new POSContext())
            {
                result = context.pos_mst_item_variant
                    .Select(x => new MstItemVariantViewModel()
                    {
                        id = x.id,
                        item_id = x.item_id,
                        name = x.name,
                        sku = x.sku,
                        price = x.price,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                    }).ToList();
            }

            return result;
        }

        public static MstItemVariantViewModel Get(int id)
        {
            MstItemVariantViewModel result = new MstItemVariantViewModel();
            using (POSContext context = new POSContext())
            {
                result = context.pos_mst_item_variant
                    .Where(x => x.id == id)
                    .Select(x => new MstItemVariantViewModel()
                    {
                        id = x.id,
                        item_id = x.item_id,
                        name = x.name,
                        sku = x.sku,
                        price = x.price,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                    }).FirstOrDefault();
            }
            return result;
        }

        //methode untuk melakukan pencarian
        public static List<MstItemVariantViewModel> Get(string keySearch)
        {
            List<MstItemVariantViewModel> result = new List<MstItemVariantViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari MstItem ke MstItemVariant
                result = context.pos_mst_item_variant
                    //.Where(x => x.NIM.ToLower().Contains(keySearch) || x.NmMstItem.ToLower().Contains(keySearch))
                    .Select(x => new MstItemVariantViewModel()
                    {
                        id = x.id,
                        item_id = x.item_id,
                        name = x.name,
                        sku = x.sku,
                        price = x.price,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,

                    }).ToList();
            }

            return result;
        }
        public static bool Insert(MstItemVariantViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // menambahkan data ke MstItem
                context.pos_mst_item_variant.Add(new pos_mst_item_variant()
                {
                    id = model.id,
                    item_id = model.item_id,
                    name = model.name,
                    sku = model.sku,
                    price = model.price,
                    created_by = model.created_by,
                    created_on = model.created_on,
                    modified_by = model.modified_by,
                    modified_on = model.modified_on,
                    active = model.active,
                });
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }

            }
            return result;
        }

        public static bool Update(MstItemVariantViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_item_variant.Find(model.id);
                // menganti nilai properti dengan model
                item.item_id = model.item_id;
                    item.name = model.name;
                    item.sku = model.sku;
                    item.price = model.price;
                    item.created_by = model.created_by;
                    item.created_on = model.created_on;
                    item.modified_by = model.modified_by;
                    item.modified_on = model.modified_on;
                    item.active = model.active;
                try 
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_item_variant.Find(id);
                // menghapus data MstItem dari object context
                context.pos_mst_item_variant.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
