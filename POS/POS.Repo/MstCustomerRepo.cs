﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POS.Model;
using POS.ViewModel;

namespace POS.Repo
{
    public class MstCustomerRepo
    {
        public static List<MstCustomerViewModel> Get()
        {
            List<MstCustomerViewModel> result = new List<MstCustomerViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari MstCustomer ke MstCustomerViewModel
                result = context.pos_mst_customer
                    .Include("pos_mst_district")
                     .Select(x => new MstCustomerViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        email = x.email,
                        phone = x.phone,
                        dob = x.dob,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                        address = x.address,
                        province_id = x.province_id,
                        namaProvinsi = x.pos_mst_province.name,
                        region_id = x.region_id,
                        namaKota = x.pos_mst_region.name,
                        district_id = x.district_id,
                        namaKecamatan = x.pos_mst_district.name
                    }).ToList();
            }

            return result;
        }

        public static List<MstCustomerViewModel> Get(string keySearch)
        {
            List<MstCustomerViewModel> result = new List<MstCustomerViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari MstCustomer ke MstCustomerViewModel
                result = context.pos_mst_customer
                    .Include ("pos_mst_district")
                    .Where(m => m.name.ToLower().Contains(keySearch) || m.email.ToLower().Contains(keySearch))
                    .Select(x => new MstCustomerViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        email = x.email,
                        phone = x.phone,
                        dob = x.dob,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                        address = x.address,
                        province_id = x.province_id,
                        namaProvinsi = x.pos_mst_province.name,
                        region_id = x.region_id,
                        namaKota = x.pos_mst_region.name,
                        district_id = x.district_id,
                        namaKecamatan = x.pos_mst_district.name

                    }).ToList();
            }

            return result;
        }

        public static MstCustomerViewModel Get(int id)
        {
            MstCustomerViewModel result = new MstCustomerViewModel();
            using (POSContext context = new POSContext())
            {
                result = context.pos_mst_customer
                    .Where(x => x.id == id)
                    .Select(x => new MstCustomerViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        email = x.email,
                        phone = x.phone,
                        dob = x.dob,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                        address = x.address,
                        province_id = x.province_id,
                        namaProvinsi = x.pos_mst_province.name,
                        region_id = x.region_id,
                        namaKota = x.pos_mst_region.name,
                        district_id = x.district_id,
                        namaKecamatan = x.pos_mst_district.name
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Insert(MstCustomerViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // menambahkan data ke MstCustomer
                context.pos_mst_customer.Add(new pos_mst_customer()
                {
                    id = model.id,
                    name = model.name,
                    email = model.email,
                    phone = model.phone,
                    dob = model.dob,
                    created_by =1,
                    created_on = DateTime.Now,
                    modified_by = 1,
                    modified_on = DateTime.Now,
                    active = "A",
                    address = model.address,
                    province_id = model.province_id,

                    region_id = model.region_id,
                    district_id = model.district_id,

                });
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }

            }
            return result;
        }

        public static bool Update(MstCustomerViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_customer.Find(model.id);
                // menganti nilai properti dengan model
                item.name = model.name;
                item.email = model.email;
                item.phone = model.phone;
                item.dob = model.dob;
                //item.created_by = model.created_by;
                //item.created_on = model.created_on;
                item.modified_by = model.modified_by;
                item.modified_on = DateTime.Now;
                //item.active = model.active;
                item.address = model.address;
                item.province_id = model.province_id;
                item.region_id = model.region_id;
                item.district_id = model.district_id;
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_role.Find(id);
                // menghapus data MstCustomer dari object context
                context.pos_mst_role.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(MstCustomerViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_role.Find(model.id);
                // menghapus data MstCustomer dari object context
                context.pos_mst_role.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

    }
}
