﻿using POS.Model;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repo
{
    public class MstOutletRepo
    {
        public static List<MstOutletViewModel> Get()
        {
            List<MstOutletViewModel> result = new List<MstOutletViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari Mahasiswa ke MstOutletViewModel
                result = context.pos_mst_outlet
                    .Select(x => new MstOutletViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        address = x.address,
                        phone = x.phone,
                        email = x.email,
                        province_id = x.province_id,
                        region_id = x.region_id,
                        district_id = x.district_id,
                        postal_code = x.postal_code,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active
                    }).ToList();
            }

            return result;
        }

        public static List<MstOutletViewModel> Get(string keySearch)
        {
            List<MstOutletViewModel> result = new List<MstOutletViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari Mahasiswa ke MstOutletViewModel
                result = context.pos_mst_outlet
                    .Where(m => m.name.ToLower().Contains(keySearch))
                    .Select(x => new MstOutletViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        address = x.address,
                        phone = x.phone,
                        email = x.email,
                        province_id = x.province_id,
                        region_id = x.region_id,
                        district_id = x.district_id,
                        postal_code = x.postal_code,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active
                    }).ToList();
            }

            return result;
        }

        public static MstOutletViewModel Get(int id)
        {
            MstOutletViewModel result = new MstOutletViewModel();
            using (POSContext context = new POSContext())
            {
                result = context.pos_mst_outlet
                    .Where(x => x.id == id)
                    .Select(x => new MstOutletViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        address = x.address,
                        phone = x.phone,
                        email = x.email,
                        province_id = x.province_id,
                        region_id = x.region_id,
                        district_id = x.district_id,
                        postal_code = x.postal_code,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Insert(MstOutletViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // menambahkan data ke Mahasiswa
                context.pos_mst_outlet.Add(new pos_mst_outlet()
                {
                    id = model.id,
                    name = model.name,
                    address = model.address,
                    phone = model.phone,
                    email = model.email,
                    province_id = model.province_id,
                    region_id = model.region_id,
                    district_id = model.district_id,
                    postal_code = model.postal_code,
                    created_by = 1,
                    created_on = DateTime.Now,
                    modified_by = 1,
                    modified_on = DateTime.Now,
                    active = "A"
                });
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }

            }
            return result;
        }

        public static bool Update(MstOutletViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_outlet.Find(model.id);
                // menganti nilai properti dengan model
                item.id = model.id;
                item.name = model.name;
                item.address = model.address;
                item.phone = model.phone;
                item.email = model.email;
                item.province_id = model.province_id;
                item.region_id = model.region_id;
                item.district_id = model.district_id;
                item.postal_code = model.postal_code;
                item.created_by = 1;
                item.created_on = DateTime.Now;
                item.modified_by = 1;
                item.modified_on = DateTime.Now;
                item.active = "A";
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_outlet.Find(id);
                // menghapus data Mahasiswa dari object context
                context.pos_mst_outlet.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(MstOutletViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_outlet.Find(model.id);
                // menghapus data Mahasiswa dari object context
                context.pos_mst_outlet.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
