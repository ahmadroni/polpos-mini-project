﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Model;
using POS.ViewModel;
using POS.Repo;

namespace POS.Web.Controllers
{
    public class MstEmployeeController : Controller
    {
        // GET: MstProvince
        public ActionResult Index()
        {
            return View(MstEmployeeRepo.Get());
        }

        public ActionResult List()
        {
            return PartialView("_List", MstEmployeeRepo.Get());
        }


        // search
        public ActionResult Search(string keySearch)
        {
            return PartialView("_List", MstEmployeeRepo.Get(keySearch));
        }

        public ActionResult AddOutlet()
        {
            ViewBag.ListOutlet = new SelectList(MstOutletRepo.Get(), "id", "name");

            return PartialView("_AddOutlet");
        }
        
        public ActionResult Add()
        {
            ViewBag.listRole = new SelectList(MstRoleRepo.Get(), "id", "name");
            ViewBag.listOutlet = new SelectList(MstOutletRepo.Get(), "id", "name");

            return PartialView("_Add");
        }


        // Add Data Submit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(MstEmployeeViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstEmployeeRepo.Insert(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Berhasil simpan"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Gagal simpan"
                    });
                }
            }

            ViewBag.listRole = new SelectList(MstRoleRepo.Get(), "id", "name");
            ViewBag.listOutlet = new SelectList(MstOutletRepo.Get(), "id", "name");

            return PartialView("_Add", model);
        }

        // detail data
        public ActionResult Detail(int id)
        {
            MstEmployeeViewModel data = MstEmployeeRepo.Get(id);
            return PartialView("_Detail", data);
        }

        // edit data
        public ActionResult Edit(int id)
        {
            //ViewBag.ListUser = new SelectList(MstUserRepo.Get(), "id", "username");
            MstEmployeeViewModel data = MstEmployeeRepo.Get(id);
            ViewBag.listRole = new SelectList(MstRoleRepo.Get(), "id", "name");
            ViewBag.listOutlet = new SelectList(MstOutletRepo.Get(), "id", "name");

            return PartialView("_Edit", data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MstEmployeeViewModel model)
        {
            //ViewBag.ListUser = new SelectList(MstUserRepo.Get(), "id", "username");
            if (ModelState.IsValid)
            {
                if (MstEmployeeRepo.Update(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Simpan Gagal"
                    });
                }
            }

            ViewBag.listRole = new SelectList(MstRoleRepo.Get(), "id", "name");
            ViewBag.listOutlet = new SelectList(MstOutletRepo.Get(), "id", "name");
            return PartialView("_Edit", model);
        }


        // delete data
        public ActionResult Delete(int id)
        {
            MstEmployeeViewModel data = MstEmployeeRepo.Get(id);
            return PartialView("_Delete", data);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(MstEmployeeViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstEmployeeRepo.Delete(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Simpan Gagal"
                    });
                }
            }
            return PartialView("_Delete", model);
        }

    }
}