﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace POS.ViewModel
{
    public class MstDistrictViewModel
    {

        public int id { get; set; }

        [Display(Name = "Region ID")]
        public int region_id { get; set; }

        //tambahan untuk nama Region

        [Display(Name = "Region Name")]
        public string regionName { get; set; }

        [Display(Name = "Distric Name")]
        public string name { get; set; }

        [Display(Name = "Created By")]
        public Nullable<int> created_by { get; set; }

        [Display(Name = "Created On")]
        public Nullable<System.DateTime> created_on { get; set; }

        [Display(Name = "Modified By")]
        public Nullable<int> modified_by { get; set; }

        [Display(Name = "Modified On")]
        public Nullable<System.DateTime> modified_on { get; set; }

        [Display(Name = "Status")]
        public string active { get; set; }

    }
}
