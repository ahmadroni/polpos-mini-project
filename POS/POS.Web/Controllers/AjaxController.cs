﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Repo;

namespace POS.Web.Controllers
{
    public class AjaxController : Controller
    {
        // GET: Ajax
        public JsonResult GetProvince()
        {
            return Json(MstProvinceRepo.Get(), JsonRequestBehavior.AllowGet);
        }


        public JsonResult GetRegion(int provinceId)
        {
            return Json(MstRegionRepo.GetByProvinsi(provinceId), JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetDistrict(int regionId)
        {
            return Json(MstDistrictRepo.GetByRegion(regionId), JsonRequestBehavior.AllowGet);
        }
    }
}