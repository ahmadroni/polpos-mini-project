﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POS.ViewModel;
using POS.Model;

namespace POS.Repo
{
    public class MstRegionRepo
    {
        public static List<MstRegionViewModel> Get()
        {
            List<MstRegionViewModel> result = new List<MstRegionViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari Kecamatan ke MstRegionViewModel
                result = context.pos_mst_region
                    .Include("pos_mst_province")
                    .Select(x => new MstRegionViewModel()
                    {
                        id = x.id,
                        province_id = x.province_id,
                        //mengambil nama region dari tabel region
                        provinceName = x.pos_mst_province.name,
                        name = x.name,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active

                    }).ToList();
            }

            return result;
        }




        public static List<MstRegionViewModel> GetByProvinsi(int provinceId)
        {
            List<MstRegionViewModel> result = new List<MstRegionViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari Kota ke KotaViewModel
                result = context.pos_mst_region
                    .Where(c => c.province_id == provinceId)
                    .Select(x => new MstRegionViewModel()
                    {
                        id = x.id,
                        province_id = x.province_id,
                        //mengambil nama region dari tabel region
                        provinceName = x.pos_mst_province.name,
                        name = x.name,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active
                    }).ToList();
            }

            return result;
        }

        public static MstRegionViewModel Get(int id)
        {
            MstRegionViewModel result = new MstRegionViewModel();
            using (POSContext context = new POSContext())
            {
                result = context.pos_mst_region
                    .Include("pos_mst_province")
                    .Where(x => x.id == id)
                    .Select(x => new MstRegionViewModel()
                    {
                        id = x.id,
                        province_id = x.province_id,
                        //mengambil nama region dari tabel region
                        provinceName = x.pos_mst_province.name,
                        name = x.name,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active
                    }).FirstOrDefault();
            }
            return result;
        }


        public static bool Insert(MstRegionViewModel x)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // menambahkan data ke Provinsi
                context.pos_mst_region.Add(new pos_mst_region()
                {
                    id = x.id,
                    province_id = x.province_id,
                    name = x.name,
                    created_by = x.created_by,
                    created_on = x.created_on,
                    modified_by = x.modified_by,
                    modified_on = x.modified_on,
                    active = x.active
                });
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }

            }
            return result;
        }

        public static bool Update(MstRegionViewModel x)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_region.Find(x.id);
                // menganti nilai properti dengan model
              
                    item.id = x.id;
                    item.province_id = x.province_id;
                    item.name = x.name;
                    item.created_by = x.created_by;
                    item.created_on = x.created_on;
                    item.modified_by = x.modified_by;
                    item.modified_on = x.modified_on;
                    item.active = x.active;
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_region.Find(id);
                // menghapus data Provinsi dari object context
                context.pos_mst_region.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
