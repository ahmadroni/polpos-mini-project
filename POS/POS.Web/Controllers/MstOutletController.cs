﻿using POS.Repo;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POS.Web.Controllers
{
    public class MstOutletController : Controller
    {
        // ajax kota
      

        // Index : MstOutlet
        public ActionResult Index()
        {
            return View(MstOutletRepo.Get());
        }

        // List : MstOutlet
        public ActionResult List()
        {
            return PartialView("_List", MstOutletRepo.Get());
        }

        // search
        public ActionResult Search(string keySearch)
        {
            return PartialView("_List", MstOutletRepo.Get(keySearch));
        }

        public ActionResult Add()
        {
            ViewBag.ListProvince = new SelectList(MstProvinceRepo.Get(), "id", "name");
            ViewBag.ListRegion = new SelectList(String.Empty, "", "");
            ViewBag.ListDistrict = new SelectList(String.Empty, "", "");
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(MstOutletViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstOutletRepo.Insert(model))
                {
                    return Json(new { success = true, message = "Data berhasil disimpan" });
                }
                else
                {
                    return Json(new { success = false, message = "Data gagal disimpan" });
                }
            }
            ViewBag.ListProvince = new SelectList(MstProvinceRepo.Get(), "id", "name");
            ViewBag.ListRegion = new SelectList(String.Empty, "", "");
            ViewBag.ListDistrict = new SelectList(String.Empty, "", "");
            return PartialView("_Add", model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.ListProvince = new SelectList(MstProvinceRepo.Get(), "id", "name");
            ViewBag.ListRegion = new SelectList(MstRegionRepo.Get(), "id", "name");
            ViewBag.ListDistrict = new SelectList(MstDistrictRepo.Get(), "id", "name");
            return PartialView("_Edit", MstOutletRepo.Get(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MstOutletViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstOutletRepo.Update(model))
                {
                    return Json(new { success = true, message = "Data berhasil disimpan" });
                }
                else
                {
                    return Json(new { success = false, message = "Data gagal disimpan" });
                }
            }
            ViewBag.ListProvince = new SelectList(MstProvinceRepo.Get(), "id", "name");
            ViewBag.ListRegion = new SelectList(MstRegionRepo.Get(), "id", "name");
            ViewBag.ListDistrict = new SelectList(MstDistrictRepo.Get(), "id", "name");
            return PartialView("_Edit", model);
        }
    }
}