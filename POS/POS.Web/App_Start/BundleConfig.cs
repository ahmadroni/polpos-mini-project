﻿using System.Web;
using System.Web.Optimization;

namespace POS.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Scripts/jquery-{version}.js",
                       "~/Scripts/jquery-ui.min.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/plugins/morris/morris.min.js",
                      "~/Scripts/plugins/sparkline/jquery.sparkline.min.js",
                      "~/Scripts/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js",
                      "~/Scripts/plugins/jvectormap/jquery-jvectormap-world-mill-en.js",
                      "~/Scripts/plugins/knob/jquery.knob.js",
                      "~/Scripts/plugins/daterangepicker/moment.min.js",
                      "~/Scripts/plugins/daterangepicker/daterangepicker.js",
                      "~/Scripts/plugins/datepicker/bootstrap-datepicker.js",
                      "~/Scripts/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js",
                      "~/Scripts/plugins/datatables/jquery.dataTables.min.js",
                      "~/Scripts/plugins/datatables/dataTables.bootstrap.min.js",
                      "~/Scripts/plugins/slimScroll/jquery.slimscroll.min.js",
                      "~/Scripts/plugins/fastclick/fastclick.js",
                      "~/Scripts/app.min.js",
                      "~/Scripts/demo.js",
                      "~/Scripts/respond.js"));

            // Login
            bundles.Add(new ScriptBundle("~/bundles/login").Include(
                    "~/Scripts/jquery-{version}.js",
                    "~/Scripts/jquery-ui.min.js",
                    "~/Scripts/bootstrap.min.js",
                    "~/Scripts/respond.js",
                    "~/Scripts/plugins/iCheck/icheck.min.js",
                    "~/Scripts/jquery.validate*",                    
                    "~/Scripts/Login.js"));

            bundles.Add(new ScriptBundle("~/bundles/register").Include(
                    "~/Scripts/jquery-{version}.js",
                    "~/Scripts/jquery-ui.min.js",
                    "~/Scripts/bootstrap.min.js",
                    "~/Scripts/respond.js",
                    "~/Scripts/plugins/iCheck/icheck.min.js",
                    "~/Scripts/jquery.validate*",
                    "~/Scripts/Register.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/font-awesome.min.css",
                      "~/Content/ionicons.min.css",
                      "~/Content/AdminLTE.min.css",
                      "~/Content/skins/_all-skins.min.css",
                      "~/Content/plugins/iCheck/flat/blue.css",
                      "~/Content/plugins/datatables/dataTables.bootstrap.css",
                      "~/Content/plugins/morris/morris.css",
                      "~/Content/plugins/jvectormap/jquery-jvectormap-1.2.2.css",
                      "~/Content/plugins/datepicker/datepicker3.css",
                      "~/Content/plugins/daterangepicker/daterangepicker.css"));
        }
    }
}
