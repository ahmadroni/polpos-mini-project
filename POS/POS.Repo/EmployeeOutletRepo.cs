﻿using System.Data.Entity;
using POS.Model;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repo
{
    public class EmployeeOutletRepo
    {
        public static List<EmployeeOutletViewModel> Get()
        {
            List<EmployeeOutletViewModel> result = new List<EmployeeOutletViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari EmployeeOutlet ke EmployeeOutletViewModel
                result = context.pos_employee_oulet
                    .Select(x => new EmployeeOutletViewModel()
                    {
                        id = x.id,
                        emoloyee_id = x.emoloyee_id,
                        outlet_id = x.outlet_id,
                        employeeName = x.pos_mst_employee.first_name,
                        outletName = x.pos_mst_outlet.name,
                    }).ToList();
            }

            return result;
        }

        public static EmployeeOutletViewModel Get(int id)
        {
            EmployeeOutletViewModel result = new EmployeeOutletViewModel();
            using (POSContext context = new POSContext())
            {
                result = context.pos_employee_oulet
                    .Where(x => x.id == id)
                    .Select(x => new EmployeeOutletViewModel()
                    {
                        id = x.id,
                        emoloyee_id = x.emoloyee_id,
                        outlet_id = x.outlet_id,
                        employeeName = x.pos_mst_employee.first_name,
                        outletName = x.pos_mst_outlet.name,
                    }).FirstOrDefault();
            }
            return result;
        }

        //methode untuk melakukan pencarian
        public static List<EmployeeOutletViewModel> Get(string keySearch)
        {
            List<EmployeeOutletViewModel> result = new List<EmployeeOutletViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari EmployeeOutlet ke EmployeeOutletViewModel
                result = context.pos_employee_oulet
                    //.Where(x => x.NIM.ToLower().Contains(keySearch) || x.NmEmployeeOutlet.ToLower().Contains(keySearch))
                    .Select(x => new EmployeeOutletViewModel()
                    {
                        id = x.id,
                        emoloyee_id = x.emoloyee_id,
                        outlet_id = x.outlet_id,
                        employeeName = x.pos_mst_employee.first_name,
                        outletName = x.pos_mst_outlet.name,
                    }).ToList();
            }

            return result;
        }
        public static bool Insert(EmployeeOutletViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // menambahkan data ke EmployeeOutlet
                context.pos_employee_oulet.Add(new pos_employee_oulet()
                {
                    id = model.id,
                    emoloyee_id = model.emoloyee_id,
                    outlet_id = model.outlet_id,
                });
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }

            }
            return result;
        }

        public static bool Update(EmployeeOutletViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_employee_oulet.Find(model.id);
                // menganti nilai properti dengan model
                    item.emoloyee_id = model.emoloyee_id;
                    item.outlet_id = model.outlet_id;
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_employee_oulet.Find(id);
                // menghapus data EmployeeOutlet dari object context
                context.pos_employee_oulet.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
