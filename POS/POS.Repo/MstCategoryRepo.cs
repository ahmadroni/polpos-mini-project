﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POS.ViewModel;
using POS.Model;

namespace POS.Repo
{
    public class MstCategoryRepo
    {
        public static List<MstCategoryViewModel> Get()
        {
            List<MstCategoryViewModel> result = new List<MstCategoryViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari MstCategory ke MstCategoryViewModel
                result = context.pos_mst_category
                    .Include("pos_mst_item")
                    .Where(x => x.active == "A")
                    .Select(x => new MstCategoryViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                        countItem = x.pos_mst_item.Count,
                    }).ToList();
            }

            return result;
        }

        public static List<MstCategoryViewModel> Get(string keySearch)
        {
            List<MstCategoryViewModel> result = new List<MstCategoryViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari MstCategory ke MstCategoryViewModel
                result = context.pos_mst_category
                    .Include("pos_mst_item")
                    .Where(m => m.name.ToLower().Contains(keySearch))
                    .Where(x => x.active == "A")
                    .Select(x => new MstCategoryViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                        countItem = x.pos_mst_item.Count,
                    }).ToList();
            }

            return result;
        }

        public static MstCategoryViewModel Get(int id)
        {
            MstCategoryViewModel result = new MstCategoryViewModel();
            using (POSContext context = new POSContext())
            {
                result = context.pos_mst_category
                    .Where(x => x.id == id)
                    .Select(x => new MstCategoryViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Insert(MstCategoryViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // menambahkan data ke MstCategory
                context.pos_mst_category.Add(new pos_mst_category()
                {
                    id = model.id,
                    name = model.name,
                    created_by = 1,
                    created_on = DateTime.Now,
                    modified_by = 1,
                    modified_on = DateTime.Now,
                    active = "A",
                });
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }

            }
            return result;
        }

        public static bool Update(MstCategoryViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_category.Find(model.id);
                // menganti nilai properti dengan model
                item.name = model.name;
                item.modified_on = DateTime.Now;
                /*
                item.created_by = model.created_by;
                item.created_on = model.created_on;
                item.modified_by = model.modified_by;
                item.active = model.active;
                 */
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_category.Find(id);
                // menghapus data MstCategory dari object context
                //context.pos_mst_role.Remove(item);
                item.active = "I";
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(MstCategoryViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_role.Find(model.id);
                // menghapus data MstCategory dari object context
                context.pos_mst_role.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
