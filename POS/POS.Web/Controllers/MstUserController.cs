﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Repo;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class MstUserController : Controller
    {
        // GET: MstUser
        public ActionResult Index()
        {
            return View(MstUserRepo.Get());
        }

        // // list Dosen
        public ActionResult List()
        {
            return PartialView("_List", MstUserRepo.Get());
        }

        // Add Data
        public ActionResult Add()
        {
            return PartialView("_Add");
        }
        // Add Data Submit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(MstUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstUserRepo.Insert(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Data berhasil disimpan"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Data gagal disimpan"
                    });
                }
            }
            return PartialView("_Add", model);
        }

        // detail data
        public ActionResult Detail(int id)
        {
            MstUserViewModel data = MstUserRepo.Get(id);
            return PartialView("_Detail", data);
        }

        // edit data
        public ActionResult Edit(int id)
        {
            ViewBag.ListRole = new SelectList(MstRoleRepo.Get(), "id", "name");
            MstUserViewModel data = MstUserRepo.Get(id);
            return PartialView("_Edit", data);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MstUserViewModel model)
        {
            ViewBag.ListRole = new SelectList(MstRoleRepo.Get(), "id", "name");
            if (ModelState.IsValid)
            {
                if (MstUserRepo.Update(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Simpan Gagal"
                    });
                }
            }
            return PartialView("_Edit", model);
        }

        // delete data
        public ActionResult Delete(int id)
        {
            MstUserViewModel data = MstUserRepo.Get(id);
            return PartialView("_Delete", data);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(MstUserViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstUserRepo.Delete(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Simpan Gagal"
                    });
                }
            }
            return PartialView("_Delete", model);
        }
    }
}