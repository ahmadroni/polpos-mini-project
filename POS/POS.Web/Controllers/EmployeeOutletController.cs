﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.ViewModel;
using POS.Repo;

namespace POS.Web.Controllers
{
    public class EmployeeOutletController : Controller
    {
        // GET: EmployeeOutlet
        public ActionResult Index()
        {
            return View(EmployeeOutletRepo.Get());
        }

        // detail mahasswa 
        public ActionResult Detail(int id)
        {
            return PartialView("_Detail", EmployeeOutletRepo.Get(id));
        }

        // list EmployeeOutlet
        public ActionResult List()
        {
            return PartialView("_List", EmployeeOutletRepo.Get());
        }

        public ActionResult Add()
        {
            ViewBag.ListMstEmployee = new SelectList(MstEmployeeRepo.Get(), "id", "first_name");
            ViewBag.ListMstOutlet = new SelectList(MstOutletRepo.Get(), "id", "name");
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(EmployeeOutletViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (EmployeeOutletRepo.Insert(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Data berhasil disimpan"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Data gagal disimpan"
                    });
                }
            }
            ViewBag.ListMstEmployee = new SelectList(MstEmployeeRepo.Get(), "id", "first_name");
            ViewBag.ListMstOutlet = new SelectList(MstOutletRepo.Get(), "id", "name");
            return PartialView("_Add", model);
        }

        // edit
        public ActionResult Edit(int id)
        {
            EmployeeOutletViewModel data = EmployeeOutletRepo.Get(id);
            ViewBag.ListMstEmployee = new SelectList(MstEmployeeRepo.Get(), "id", "first_name");
            ViewBag.ListMstOutlet = new SelectList(MstOutletRepo.Get(), "id", "name");

            return PartialView("_Edit", data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(EmployeeOutletViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (EmployeeOutletRepo.Update(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Data berhasil disimpan"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Data gagal disimpan"
                    });
                }
            }
            ViewBag.ListMstEmployee = new SelectList(MstEmployeeRepo.Get(), "id", "first_name");
            ViewBag.ListMstOutlet = new SelectList(MstOutletRepo.Get(), "id", "name");
            return PartialView("_Edit", model);
        }

        public ActionResult Delete(int id)
        {
            EmployeeOutletViewModel data = EmployeeOutletRepo.Get(id);
            return PartialView("_Delete", data);
        }
        /*
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id)
        {
            if (ModelState.IsValid)
            {
                if (EmployeeOutletRepo.Delete(id))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Simpan Gagal"
                    });
                }
            }
            return PartialView("_Delete", model);
        }
         * */
    }
}