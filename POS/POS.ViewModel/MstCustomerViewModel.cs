﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.ViewModel
{
    public class MstCustomerViewModel
    {
        public int id { get; set; }
        [Display(Name = "Customer Name")]
        public string name { get; set; }
        [Display(Name = "Email")]
        public string email { get; set; }
        [Display(Name = "Phone")]
        public string phone { get; set; }
        [Display(Name = "Date of Birthday")]
        public Nullable<System.DateTime> dob { get; set; }
        [Display(Name = "Address")]
        public string address { get; set; }
        public int province_id { get; set; }
        [Display(Name = "Province Name")]
        public string namaProvinsi { get; set; }
        public int region_id { get; set; }
        [Display(Name = "Region Name")]
        public string  namaKota { get; set; }
        public int district_id { get; set; }
        [Display(Name = "District Name")]
        public string  namaKecamatan { get; set; }

        public Nullable<int> created_by { get; set; }
        public Nullable<System.DateTime> created_on { get; set; }
        public Nullable<int> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public string active { get; set; }
    }
}
