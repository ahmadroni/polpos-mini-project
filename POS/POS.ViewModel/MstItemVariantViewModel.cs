﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.ViewModel
{
    public class MstItemVariantViewModel
    {
        public int id { get; set; }
        public int item_id { get; set; }
        public string name { get; set; }
        public string sku { get; set; }
        public double price { get; set; }
        public int begining_stok { get; set; }
        public int alert_at { get; set; }
        public Nullable<int> created_by { get; set; }
        public Nullable<System.DateTime> created_on { get; set; }
        public Nullable<int> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public string active { get; set; }
    }
}
