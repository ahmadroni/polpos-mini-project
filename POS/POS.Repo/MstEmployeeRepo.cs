﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POS.Model;
using POS.ViewModel;

namespace POS.Repo
{
    public class MstEmployeeRepo
    {
        public static List<MstEmployeeViewModel> Get()
        {
            List<MstEmployeeViewModel> result = new List<MstEmployeeViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari MstItem ke MstEmployeeViewModel
                result = context.pos_mst_employee
                    .Include("Category")
                    .Select(x => new MstEmployeeViewModel()
                    {
                        id = x.id,
                        first_name = x.first_name,
                        last_name = x.last_name,
                        email = x.email,
                        title = x.title,
                        have_account = x.have_account,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.created_on,
                        active = x.active
                    }).ToList();
            }

            return result;
        }

        public static MstEmployeeViewModel Get(int id)
        {
            MstEmployeeViewModel result = new MstEmployeeViewModel();
            using (POSContext context = new POSContext())
            {
                result = context.pos_mst_employee

                    .Where(x => x.id == id)
                    .Select(x => new MstEmployeeViewModel()
                    {
                        id = x.id,
                        first_name = x.first_name,
                        last_name = x.last_name,
                        email = x.email,
                        title = x.title,
                        have_account = x.have_account,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.created_on,
                        active = x.active
                    }).FirstOrDefault();
            }
            return result;
        }

        //methode untuk melakukan pencarian
        public static List<MstEmployeeViewModel> Get(string keySearch)
        {
            List<MstEmployeeViewModel> result = new List<MstEmployeeViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari MstItem ke MstEmployeeViewModel
                result = context.pos_mst_employee
                    //.Where(x => x.NIM.ToLower().Contains(keySearch) || x.NmMstItem.ToLower().Contains(keySearch))
                    .Select(x => new MstEmployeeViewModel()
                    {
                        id = x.id,
                        first_name = x.first_name,
                        last_name = x.last_name,
                        email = x.email,
                        title = x.title,
                        have_account = x.have_account,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.created_on,
                        active = x.active

                    }).ToList();
            }

            return result;
        }
        public static bool Insert(MstEmployeeViewModel x)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // menambahkan data ke MstItem
                context.pos_mst_employee.Add(new pos_mst_employee()
                {
                    id = x.id,
                    first_name = x.first_name,
                    last_name = x.last_name,
                    email = x.email,
                    title = x.title,
                    have_account = x.have_account,
                    created_by = x.created_by,
                    created_on = x.created_on,
                    modified_by = x.modified_by,
                    modified_on = x.created_on,
                    active = "A"
                });
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }

            }
            return result;
        }

        public static bool Update(MstEmployeeViewModel x)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {

                       
                // ambil dulu dari Database
                var item = context.pos_mst_employee.Find(x.id);
                // menganti nilai properti dengan model

                 
                     item.id = x.id;
                        item.last_name = x.last_name;
                        item.email = x.email;
                        item.title = x.title;
                        item.have_account = x.have_account;
                        item.created_by = x.created_by;
                        item.created_on = x.created_on;
                        item.modified_by = x.modified_by;
                        item.modified_on = x.created_on;
                        item.active = x.active;

            
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_employee.Find(id);
                // menghapus data MstItem dari object context
                context.pos_mst_employee.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(MstEmployeeViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_employee.Find(model.id);
                // menghapus data MstUser dari object context
                context.pos_mst_employee.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }


    }
}
