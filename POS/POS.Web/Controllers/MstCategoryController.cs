﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Repo;
using POS.ViewModel;


namespace POS.Web.Controllers
{
    public class MstCategoryController : Controller
    {
        // GET: Category
        public ActionResult Index()
        {
            return View(MstCategoryRepo.Get());
        }

        public ActionResult List()
        {
            return PartialView("_List", MstCategoryRepo.Get());
        }

        // search
        public ActionResult Search(string keySearch)
        {
            return PartialView("_List", MstCategoryRepo.Get(keySearch));
        }

        public ActionResult Add()
        {
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(MstCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstCategoryRepo.Insert(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Simpan Gagal"
                    });
                }
            }
            return PartialView("_Add", model);
        }

        public ActionResult Edit(int id)
        {
            MstCategoryViewModel data = MstCategoryRepo.Get(id);
            return PartialView("_Edit", data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MstCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstCategoryRepo.Update(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Simpan Gagal"
                    });
                }
            }
            return PartialView("_Edit", model);
        }

        public ActionResult Details(int id)
        {
            MstCategoryViewModel data = MstCategoryRepo.Get(id);
            return PartialView("_Details", data);
        }

        public JsonResult Delete(int id)
        {
            if (MstCategoryRepo.Delete(id))
            {
                return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = "Simpan Gagal"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(MstCategoryViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstCategoryRepo.Delete(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Simpan Gagal"
                    });
                }
            }
            return PartialView("_Delete", model);
        }
    }
}