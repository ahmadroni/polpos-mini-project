﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.ViewModel
{
    public class MstItemViewModel
    {
        public int id { get; set; }
        [Display (Name = "Name")]
        public string name { get; set; }
        [Display(Name = "Category")]
        public Nullable<int> category_id { get; set; }
        [Display(Name = "Create By")]
        public Nullable<int> created_by { get; set; }
        [Display(Name = "Created On")]
        public Nullable<DateTime> created_on { get; set; }
        [Display(Name = "Modified By")]
        public Nullable<int> modified_by { get; set; }
        [Display(Name = "Modified On")]
        public Nullable<System.DateTime> modified_on { get; set; }
        [Display(Name = "Active")]
        public string active { get; set; }

        public List<MstItemVariantViewModel> Detail { get; set; }
    }
}
