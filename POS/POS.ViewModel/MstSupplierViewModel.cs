﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.ViewModel
{
    public class MstSupplierViewModel
    {
        [Required]
        public int id { get; set; }
        [Display (Name="Name")]
        [Required]
        public string name { get; set; }
        [Display(Name = "Address")]
        public string address { get; set; }
        [Display(Name = "Phone")]
        public string phone { get; set; }
        [Display(Name = "Email")]
        public string email { get; set; }
        [Required]
        public int province_id { get; set; }
        [Required]
        public int region_id { get; set; }
        [Required]
        public int district_id { get; set; }
        public string postal_code { get; set; }
        public Nullable<int> created_by { get; set; }
        public Nullable<System.DateTime> created_on { get; set; }
        public Nullable<int> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public string active { get; set; }
    }
}
