﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace POS.ViewModel
{
    public class MstEmployeeViewModel
    {

        public int id { get; set; }

         [Display(Name = "First Name")]
        public string first_name { get; set; }

         [Display(Name = "Last Name")]
        public string last_name { get; set; }

         [Display(Name = "Email")]
        public string email { get; set; }

         [Display(Name = "Title")]
        public string title { get; set; }

         [Display(Name = "Have account?")]
        public string have_account { get; set; }

         [Display(Name = "Role")]
         public int roleId { get; set; }

         [Display(Name = "Username")]
         public string username { get; set; }

         [Display(Name = "Password")]
         public string password { get; set; }

         [Display(Name = "Outlet")]
         public List<EmployeeOutletViewModel> EmployeeOutlet { get; set; }

         [Display(Name = "Created By")]
        public Nullable<int> created_by { get; set; }

         [Display(Name = "Created On")]
        public Nullable<System.DateTime> created_on { get; set; }

         [Display(Name = "Modified By")]
        public Nullable<int> modified_by { get; set; }

         [Display(Name = "Modified On")]
        public Nullable<System.DateTime> modified_on { get; set; }

         [Display(Name = "Status")]
        public string active { get; set; }

         public List<MstRoleViewModel> ListRole { get; set; }
         public List<MstOutletViewModel> ListOutlet { get; set; }

    }
}
