﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POS.ViewModel;
using POS.Model;

namespace POS.Repo
{
    public class MstProvinceRepo
    {
        public static List<MstProvinceViewModel> Get()
        {
            List<MstProvinceViewModel> result = new List<MstProvinceViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari MstProvince ke MstProvinceViewModel
                result = context.pos_mst_province
                    .Select(x => new MstProvinceViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active


                    }).ToList();
            }

            return result;
        }

        public static MstProvinceViewModel Get(int id)
        {
            MstProvinceViewModel result = new MstProvinceViewModel();
            using (POSContext context = new POSContext())
            {
                result = context.pos_mst_province
                    .Where(x => x.id == id)
                    .Select(x => new MstProvinceViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active

                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Insert(MstProvinceViewModel x)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // menambahkan data ke Provinsi
                context.pos_mst_province.Add(new pos_mst_province()
                {
                    id = x.id,
                    name = x.name,
                    created_by = x.created_by,
                    created_on = x.created_on,
                    modified_by = x.modified_by,
                    modified_on = x.modified_on,
                    active = x.active
                });
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }

            }
            return result;
        }

       

        public static bool Update(MstProvinceViewModel x)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_province.Find(x.id);
                // menganti nilai properti dengan model

                    item.id = x.id;
                    item.name = x.name;
                    item.created_by = x.created_by;
                    item.created_on = x.created_on;
                    item.modified_by = x.modified_by;
                    item.modified_on = x.modified_on;
                    item.active = x.active;

                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_province.Find(id);
                // menghapus data MstProvince dari object context
                context.pos_mst_province.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
