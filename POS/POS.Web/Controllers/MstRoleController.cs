﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Repo;
using POS.ViewModel;

namespace POS.Web.Controllers
{
    public class MstRoleController : Controller
    {
        // GET: MstRole
        public ActionResult Index()
        {
            return View(MstRoleRepo.Get());
        }

        // // list Dosen
        public ActionResult List()
        {
            return PartialView("_List", MstRoleRepo.Get());
        }

        // search
        public ActionResult Search(string keySearch)
        {
            return PartialView("_List", MstRoleRepo.Get(keySearch));
        }

        // Add Data
        public ActionResult Add()
        {
            return PartialView("_Add");
        }
        // Add Data Submit
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(MstRoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstRoleRepo.Insert(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Data berhasil disimpan"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Data gagal disimpan"
                    });
                }
            }
            return PartialView("_Add", model);
        }

        // detail data
        public ActionResult Detail(int id)
        {
            MstRoleViewModel data = MstRoleRepo.Get(id);
            return PartialView("_Detail", data);
        }

        // edit data
        public ActionResult Edit(int id)
        {
            ViewBag.ListUser = new SelectList(MstUserRepo.Get(), "id", "username");
            MstRoleViewModel data = MstRoleRepo.Get(id);
            return PartialView("_Edit", data);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MstRoleViewModel model)
        {
            ViewBag.ListUser = new SelectList(MstUserRepo.Get(), "id", "username");
            if (ModelState.IsValid)
            {
                if (MstRoleRepo.Update(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Simpan Gagal"
                    });
                }
            }
            return PartialView("_Edit", model);
        }

        // delete data
        public ActionResult Delete(int id)
        {
            MstRoleViewModel data = MstRoleRepo.Get(id);
            return PartialView("_Delete", data);
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(MstRoleViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstRoleRepo.Delete(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Simpan Gagal"
                    });
                }
            }
            return PartialView("_Delete", model);
        }

    }



}