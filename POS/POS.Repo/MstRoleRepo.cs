﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POS.ViewModel;
using POS.Model;

namespace POS.Repo
{
    public class MstRoleRepo
    {
        public static List<MstRoleViewModel> Get()
        {
            List<MstRoleViewModel> result = new List<MstRoleViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari MstRole ke MstRoleViewModel
                result = context.pos_mst_role
                    .Select(x => new MstRoleViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        description = x.description,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                    }).ToList();
            }

            return result;
        }

        public static List<MstRoleViewModel> Get(string keySearch)
        {
            List<MstRoleViewModel> result = new List<MstRoleViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari MstRole ke MstRoleViewModel
                result = context.pos_mst_role
                    .Where(m => m.name.ToLower().Contains(keySearch))
                    .Select(x => new MstRoleViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        description = x.description,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                    }).ToList();
            }

            return result;
        }

        public static MstRoleViewModel Get(int id)
        {
            MstRoleViewModel result = new MstRoleViewModel();
            using (POSContext context = new POSContext())
            {
                result = context.pos_mst_role
                    .Where(x => x.id == id)
                    .Select(x => new MstRoleViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        description = x.description,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Insert(MstRoleViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // menambahkan data ke MstRole
                context.pos_mst_role.Add(new pos_mst_role()
                {
                    id = model.id,
                    name = model.name,
                    description = model.description,
                    created_by = model.created_by,
                    created_on = DateTime.Now,
                    modified_by = model.modified_by,
                    modified_on = model.modified_on,
                    active = model.active,
                });
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }

            }
            return result;
        }

        public static bool Update(MstRoleViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_role.Find(model.id);
                // menganti nilai properti dengan model
                item.name = model.name;
                item.description = model.description;
                item.created_by = model.created_by;
                item.created_on = model.created_on;
                item.modified_by = model.modified_by;
                item.modified_on = model.modified_on;
                item.active = model.active;
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_role.Find(id);
                // menghapus data MstRole dari object context
                context.pos_mst_role.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(MstRoleViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_role.Find(model.id);
                // menghapus data MstRole dari object context
                context.pos_mst_role.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
