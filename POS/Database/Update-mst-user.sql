USE [MINIPROJECT]
GO

ALTER TABLE [dbo].[pos_mst_user] DROP CONSTRAINT [FK_pos_mst_user_pos_mst_role]
GO

ALTER TABLE [dbo].[pos_mst_user] DROP CONSTRAINT [FK_pos_mst_user_pos_mst_employee]
GO

/****** Object:  Table [dbo].[pos_mst_user]    Script Date: 9/26/2017 10:24:36 AM ******/
DROP TABLE [dbo].[pos_mst_user]
GO

/****** Object:  Table [dbo].[pos_mst_user]    Script Date: 9/26/2017 10:24:36 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[pos_mst_user](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[username] [varchar](50) NOT NULL,
	[password] [varchar](255) NOT NULL,
	[role_id] [int] NOT NULL,
	[employee_id] [int] NOT NULL,
	[created_by] [int] NULL,
	[created_on] [datetime] NULL,
	[modified_by] [int] NULL,
	[modified_on] [datetime] NULL,
	[active] [varchar](10) NOT NULL,
 CONSTRAINT [PK_pos_mst_user] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[pos_mst_user]  WITH CHECK ADD  CONSTRAINT [FK_pos_mst_user_pos_mst_employee] FOREIGN KEY([employee_id])
REFERENCES [dbo].[pos_mst_employee] ([id])
GO

ALTER TABLE [dbo].[pos_mst_user] CHECK CONSTRAINT [FK_pos_mst_user_pos_mst_employee]
GO

ALTER TABLE [dbo].[pos_mst_user]  WITH CHECK ADD  CONSTRAINT [FK_pos_mst_user_pos_mst_role] FOREIGN KEY([role_id])
REFERENCES [dbo].[pos_mst_role] ([id])
GO

ALTER TABLE [dbo].[pos_mst_user] CHECK CONSTRAINT [FK_pos_mst_user_pos_mst_role]
GO


