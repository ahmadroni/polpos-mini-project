﻿using POS.Repo;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace POS.Web.Controllers
{
    public class MstItemController : Controller
    {
        // GET: MstItem
        public ActionResult Index()
        {
            return View(MstItemRepo.Get());
        }

        public ActionResult Add()
        {
            ViewBag.ListCategory = new SelectList(MstCategoryRepo.Get(),"id","name");
            return PartialView("_Add");
        }

        [HttpPost]
        public ActionResult Add(MstItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstItemRepo.Insert(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Data berhasil disimpan"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Data gagal disimpan"
                    });
                }
            }
            ViewBag.ListCategory = new SelectList(MstCategoryRepo.Get(), "id", "name");
            return View(model);

        }

        public ActionResult AddVariant()
        {
            return PartialView("_AddVariant");
        }


        public ActionResult Edit(int id)
        {
            MstItemViewModel data = MstItemRepo.Get(id);
            //@ViewBag.ListCategory = new SelectList(MstCategoryRepo.Get(),"id","name");
            return PartialView("_Edit",data);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MstItemViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstItemRepo.Update(model))
                {
                    return Json(new
                    {
                        success = true,
                        messege = "Berhasil simpan"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        messege = "Gagal"
                    });
                }
            }
            return PartialView("_Edit", model);
        }

        public ActionResult List()
        {
            return PartialView("_List", MstItemRepo.Get());
        }

        public ActionResult Detail(int id)
        {
            return PartialView("_Detail", MstItemRepo.Get(id));
        }

    }
}