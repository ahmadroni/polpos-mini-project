USE [MiniProject]
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_pos_mst_item_pos_mst_category]') AND parent_object_id = OBJECT_ID(N'[dbo].[pos_mst_item]'))
ALTER TABLE [dbo].[pos_mst_item] DROP CONSTRAINT [FK_pos_mst_item_pos_mst_category]
GO

USE [MiniProject]
GO

/****** Object:  Table [dbo].[pos_mst_item]    Script Date: 09/26/2017 10:24:56 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[pos_mst_item]') AND type in (N'U'))
DROP TABLE [dbo].[pos_mst_item]
GO

USE [MiniProject]
GO

/****** Object:  Table [dbo].[pos_mst_item]    Script Date: 09/26/2017 10:24:56 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE [dbo].[pos_mst_item](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](255) NULL,
	[category_id] [int] NULL,
	[created_by] [int] NULL,
	[created_on] [datetime] NULL,
	[modified_by] [int] NULL,
	[modified_on] [datetime] NULL,
	[active] [varchar](10) NOT NULL,
 CONSTRAINT [PK_pos_mst_item] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[pos_mst_item]  WITH CHECK ADD  CONSTRAINT [FK_pos_mst_item_pos_mst_category] FOREIGN KEY([category_id])
REFERENCES [dbo].[pos_mst_category] ([id])
GO

ALTER TABLE [dbo].[pos_mst_item] CHECK CONSTRAINT [FK_pos_mst_item_pos_mst_category]
GO


