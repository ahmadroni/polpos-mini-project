﻿using System.Data.Entity;
using POS.Model;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repo
{
    public class MstItemRepo
    {
        public static List<MstItemViewModel> Get()
        {
            List<MstItemViewModel> result = new List<MstItemViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari MstItem ke MstItemViewModel
                result = context.pos_mst_item
                    .Include("Category")
                    .Select(x => new MstItemViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        category_id = x.category_id,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                    }).ToList();
            }

            return result;
        }

        public static MstItemViewModel Get(int id)
        {
            MstItemViewModel result = new MstItemViewModel();
            using (POSContext context = new POSContext())
            {
                result = context.pos_mst_item
                    .Include("Category")
                    .Where(x => x.id == id)
                    .Select(x => new MstItemViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        category_id = x.category_id,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                    }).FirstOrDefault();
            }
            return result;
        }

        //methode untuk melakukan pencarian
        public static List<MstItemViewModel> Get(string keySearch)
        {
            List<MstItemViewModel> result = new List<MstItemViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari MstItem ke MstItemViewModel
                result = context.pos_mst_item
                    //.Where(x => x.NIM.ToLower().Contains(keySearch) || x.NmMstItem.ToLower().Contains(keySearch))
                    .Select(x => new MstItemViewModel()
                    {
                        id = x.id,
                        name = x.name,
                        category_id = x.category_id,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,

                    }).ToList();
            }

            return result;
        }
        public static bool Insert(MstItemViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // menambahkan data ke MstItem
                pos_mst_item item = new pos_mst_item()
                {
                    id = model.id,
                    name = model.name,
                    category_id = model.category_id,
                    created_by = 1,
                    created_on = DateTime.Now,
                    modified_by = 1,
                    modified_on = DateTime.Now,
                    active = "A"
                };
                context.pos_mst_item.Add(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    foreach (var itemDetail in model.Detail)
                    {
                        pos_mst_item_variant items = new pos_mst_item_variant()
                        {
                            item_id = item.id,
                            name = itemDetail.name,
                            sku = itemDetail.sku,
                            price = itemDetail.price,
                            created_by = 1,
                            created_on = DateTime.Now,
                            modified_by = 1,
                            modified_on = DateTime.Now,
                            active = "A"
                        };
                        context.pos_mst_item_variant.Add(items);
                        context.SaveChanges();



                        //untuk menyimpan ke tabel item_inventory
                        pos_item_inventory inventory = new pos_item_inventory()
                        {
                            variant_id = items.id,
                            outlet_id = 1,
                            beginning = itemDetail.begining_stok,
                            purchase_qty = 0,
                            sales_order_qty = 0,
                            transfer_stock_qty = 0,
                            adjustment_qty = 0,
                            ending_qty = itemDetail.begining_stok,
                            alert_at_qty = itemDetail.alert_at,
                            created_by = 1,
                            created_on = DateTime.Now,
                            modified_by = 1,
                            modified_on = DateTime.Now,
                        };
                        context.pos_item_inventory.Add(inventory);

                        context.SaveChanges();
                    }
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }

            }
            return result;
        }

        public static bool Update(MstItemViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_item.Find(model.id);
                // menganti nilai properti dengan model
                item.id = model.id;
                item.name = model.name;
                item.category_id = model.category_id;
                item.created_by = 1;
                item.created_on = DateTime.Now;
                item.modified_by = 1;
                item.modified_on = DateTime.Now;
                item.active = "A";
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_item.Find(id);
                // menghapus data MstItem dari object context
                context.pos_mst_item.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
