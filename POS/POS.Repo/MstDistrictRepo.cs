﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POS.Model;
using POS.ViewModel;

namespace POS.Repo
{
    public class MstDistrictRepo
    {
        public static List<MstDistrictViewModel> Get()
        {
            List<MstDistrictViewModel> result = new List<MstDistrictViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari Kecamatan ke MstDistrictViewModel
                result = context.pos_mst_district
                    .Include("pos_mst_region")
                    .Select(x => new MstDistrictViewModel()
                    {

                        id = x.id,
                        region_id = x.region_id,
                        //mengambil nama region dari tabel region
                        regionName = x.pos_mst_region.name,

                        name = x.name,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active

                       

                    }).ToList();
            }

            return result;
        }

        public static List<MstDistrictViewModel> GetByRegion(int regionId)
        {
            List<MstDistrictViewModel> result = new List<MstDistrictViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari Kota ke KotaViewModel
                result = context.pos_mst_district
                    .Where(c => c.region_id == regionId)
                    .Select(x => new MstDistrictViewModel()
                    {
                        id = x.id,
                        region_id = x.region_id,
                        //mengambil nama region dari tabel region
                        regionName = x.pos_mst_region.name,
                        name = x.name,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active
                    }).ToList();
            }

            return result;
        }

        public static MstDistrictViewModel Get(int id)
        {
            MstDistrictViewModel result = new MstDistrictViewModel();
            using (POSContext context = new POSContext())
            {
                result = context.pos_mst_district
                    .Include("pos_mst_region")
                    .Where(x => x.id == id)
                    .Select(x => new MstDistrictViewModel()
                    {
                        id = x.id,
                        region_id = x.region_id,
                        //mengambil nama region dari tabel region
                        regionName = x.pos_mst_region.name,

                        name = x.name,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Insert(MstDistrictViewModel x)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // menambahkan data ke Kecamatan
                context.pos_mst_district.Add(new pos_mst_district()
                {
                    id = x.id,
                    region_id = x.region_id,
                    name = x.name,
                    created_by = x.created_by,
                    created_on = x.created_on,
                    modified_by = x.modified_by,
                    modified_on = x.modified_on,
                    active = x.active
                });
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }

            }
            return result;
        }

        public static bool Update(MstDistrictViewModel x)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_district.Find(x.id);
                // menganti nilai properti dengan model

                    item.id = x.id;
                    item.region_id = x.region_id;
                    item.name = x.name;
                    item.created_by = x.created_by;
                    item.created_on = x.created_on;
                    item.modified_by = x.modified_by;
                    item.modified_on = x.modified_on;
                    item.active = x.active;

               

                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_district.Find(id);
                // menghapus data Kecamatan dari object context
                context.pos_mst_district.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(MstDistrictViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_district.Find(model.id);
                // menghapus data Kecamatan dari object context
                context.pos_mst_district.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
