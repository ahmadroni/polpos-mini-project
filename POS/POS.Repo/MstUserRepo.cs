﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using POS.ViewModel;
using POS.Model;

namespace POS.Repo
{
    public class MstUserRepo
    {
        public static List<MstUserViewModel> Get()
        {
            List<MstUserViewModel> result = new List<MstUserViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari MstUser ke MstUserViewModel
                result = context.pos_mst_user
                    .Select(x => new MstUserViewModel()
                    {
                        id = x.id,
                        username = x.username,
                        password = x.password,
                        role_id = x.role_id,
                        employee_id = x.employee_id,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                    }).ToList();
            }

            return result;
        }

        public static List<MstUserViewModel> Get(string keySearch)
        {
            List<MstUserViewModel> result = new List<MstUserViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari MstUser ke MstUserViewModel
                result = context.pos_mst_user
                    .Where(m => m.username.ToLower().Contains(keySearch) || m.password.ToLower().Contains(keySearch))
                    .Select(x => new MstUserViewModel()
                    {
                        id = x.id,
                        username = x.username,
                        password = x.password,
                        role_id = x.role_id,
                        employee_id = x.employee_id,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                    }).ToList();
            }

            return result;
        }

        public static MstUserViewModel Get(int id)
        {
            MstUserViewModel result = new MstUserViewModel();
            using (POSContext context = new POSContext())
            {
                result = context.pos_mst_user
                    .Where(x => x.id == id)
                    .Select(x => new MstUserViewModel()
                    {
                        id = x.id,
                        username = x.username,
                        password = x.password,
                        role_id = x.role_id,
                        employee_id = x.employee_id,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on,
                        active = x.active,
                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Insert(MstUserViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // menambahkan data ke MstUser
                context.pos_mst_user.Add(new pos_mst_user()
                {
                    id = model.id,
                    username = model.username,
                    password = model.password,
                    role_id = model.role_id,
                    employee_id = model.employee_id,
                    created_by = model.created_by,
                    created_on = DateTime.Now,
                    modified_by = model.modified_by,
                    modified_on = model.modified_on,
                    active = model.active,
                });
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }

            }
            return result;
        }

        public static bool Update(MstUserViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_user.Find(model.id);
                // menganti nilai properti dengan model
                item.username = model.username;
                item.password = model.password;
                item.role_id = model.role_id;
                item.employee_id = model.employee_id;
                item.created_by = model.created_by;
                item.created_on = model.created_on;
                item.modified_by = model.modified_by;
                item.modified_on = model.modified_on;
                item.active = model.active;
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_role.Find(id);
                // menghapus data MstUser dari object context
                context.pos_mst_role.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(MstUserViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_mst_role.Find(model.id);
                // menghapus data MstUser dari object context
                context.pos_mst_role.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
