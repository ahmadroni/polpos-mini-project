﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.ViewModel
{
    public class ItemInventoryViewModel
    {
        [Required]
        public int id { get; set; }
        public Nullable<int> variant_id { get; set; }
        [Required]
        public int outlet_id { get; set; }
        [Required]
        public int beginning { get; set; }
        public Nullable<int> purchase_qty { get; set; }
        public Nullable<int> sales_order_qty { get; set; }
        public Nullable<int> transfer_stock_qty { get; set; }
        public Nullable<int> adjustment_qty { get; set; }
        [Required]
        public int ending_qty { get; set; }
        [Required]
        public int alert_at_qty { get; set; }
        public Nullable<int> created_by { get; set; }
        public Nullable<System.DateTime> created_on { get; set; }
        public Nullable<int> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
    }
}
