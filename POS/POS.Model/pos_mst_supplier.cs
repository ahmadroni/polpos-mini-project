//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace POS.Model
{
    using System;
    using System.Collections.Generic;
    
    public partial class pos_mst_supplier
    {
        public int id { get; set; }
        public string name { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string email { get; set; }
        public int province_id { get; set; }
        public int region_id { get; set; }
        public int district_id { get; set; }
        public string postal_code { get; set; }
        public Nullable<int> created_by { get; set; }
        public Nullable<System.DateTime> created_on { get; set; }
        public Nullable<int> modified_by { get; set; }
        public Nullable<System.DateTime> modified_on { get; set; }
        public string active { get; set; }
    
        public virtual pos_mst_district pos_mst_district { get; set; }
        public virtual pos_mst_province pos_mst_province { get; set; }
        public virtual pos_mst_region pos_mst_region { get; set; }
    }
}
