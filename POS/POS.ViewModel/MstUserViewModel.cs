﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.ViewModel
{
    public class MstUserViewModel
    {
        public int id { get; set; }
        [Display(Name="Username")]
        public string username { get; set; }
        [Display(Name = "Password")]
        public string password { get; set; }
        public int role_id { get; set; }
        public int employee_id { get; set; }
        [Display(Name = "Dibuat Oleh")]
        public Nullable<int> created_by { get; set; }
        [Display(Name = "Dibuat Tanggal")]
        public Nullable<System.DateTime> created_on { get; set; }
        [Display(Name = "Dimodifikasi Oleh")]
        public Nullable<int> modified_by { get; set; }
        [Display(Name = "Dimodifikasi Tanggal")]
        public Nullable<System.DateTime> modified_on { get; set; }
        [Display(Name = "Status Aktivasi")]
        public string active { get; set; }
    }
}
