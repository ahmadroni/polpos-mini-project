﻿using POS.Model;
using POS.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.Repo
{
    public class ItemInventoryRepo
    {
        public static List<ItemInventoryViewModel> Get()
        {
            List<ItemInventoryViewModel> result = new List<ItemInventoryViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari Mahasiswa ke ItemInventoryViewModel
                result = context.pos_item_inventory
                    .Select(x => new ItemInventoryViewModel()
                    {
                        id = x.id,
                        variant_id = x.variant_id,
                        outlet_id = x.outlet_id,
                        beginning = x.beginning,
                        purchase_qty = x.purchase_qty,
                        sales_order_qty = x.sales_order_qty,
                        transfer_stock_qty = x.transfer_stock_qty,
                        adjustment_qty = x.adjustment_qty,
                        ending_qty = x.ending_qty,
                        alert_at_qty = x.alert_at_qty,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on
                    }).ToList();
            }

            return result;
        }

        public static List<ItemInventoryViewModel> Get(string keySearch)
        {
            List<ItemInventoryViewModel> result = new List<ItemInventoryViewModel>();
            using (POSContext context = new POSContext())
            {
                // mengkonvert atau casting dari Mahasiswa ke ItemInventoryViewModel
                result = context.pos_item_inventory
                    .Select(x => new ItemInventoryViewModel()
                    {
                        id = x.id,
                        variant_id = x.variant_id,
                        outlet_id = x.outlet_id,
                        beginning = x.beginning,
                        purchase_qty = x.purchase_qty,
                        sales_order_qty = x.sales_order_qty,
                        transfer_stock_qty = x.transfer_stock_qty,
                        adjustment_qty = x.adjustment_qty,
                        ending_qty = x.ending_qty,
                        alert_at_qty = x.alert_at_qty,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on

                    }).ToList();
            }

            return result;
        }

        public static ItemInventoryViewModel Get(int id)
        {
            ItemInventoryViewModel result = new ItemInventoryViewModel();
            using (POSContext context = new POSContext())
            {
                result = context.pos_item_inventory
                    .Where(x => x.id == id)
                    .Select(x => new ItemInventoryViewModel()
                    {
                        id = x.id,
                        variant_id = x.variant_id,
                        outlet_id = x.outlet_id,
                        beginning = x.beginning,
                        purchase_qty = x.purchase_qty,
                        sales_order_qty = x.sales_order_qty,
                        transfer_stock_qty = x.transfer_stock_qty,
                        adjustment_qty = x.adjustment_qty,
                        ending_qty = x.ending_qty,
                        alert_at_qty = x.alert_at_qty,
                        created_by = x.created_by,
                        created_on = x.created_on,
                        modified_by = x.modified_by,
                        modified_on = x.modified_on

                    }).FirstOrDefault();
            }
            return result;
        }

        public static bool Insert(ItemInventoryViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // menambahkan data ke Mahasiswa
                context.pos_item_inventory.Add(new pos_item_inventory()
                {
                    id = model.id,
                    variant_id = model.variant_id,
                    outlet_id = model.outlet_id,
                    beginning = model.beginning,
                    purchase_qty = model.purchase_qty,
                    sales_order_qty = model.sales_order_qty,
                    transfer_stock_qty = model.transfer_stock_qty,
                    adjustment_qty = model.adjustment_qty,
                    ending_qty = model.ending_qty,
                    alert_at_qty = model.alert_at_qty,
                    created_by = 1,
                    created_on = DateTime.Now,
                    modified_by = 1,
                    modified_on = DateTime.Now,

                });
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }

            }
            return result;
        }

        public static bool Update(ItemInventoryViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_item_inventory.Find(model.id);
                // menganti nilai properti dengan model
                item.id = model.id;
                item.variant_id = model.variant_id;
                item.outlet_id = model.outlet_id;
                item.beginning = model.beginning;
                item.purchase_qty = model.purchase_qty;
                item.sales_order_qty = model.sales_order_qty;
                item.transfer_stock_qty = model.transfer_stock_qty;
                item.adjustment_qty = model.adjustment_qty;
                item.ending_qty = model.ending_qty;
                item.alert_at_qty = model.alert_at_qty;
                item.created_by = 1;
                item.created_on = DateTime.Now;
                item.modified_by = 1;
                item.modified_on = DateTime.Now;

                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(int id)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_item_inventory.Find(id);
                // menghapus data Mahasiswa dari object context
                context.pos_item_inventory.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }

        public static bool Delete(ItemInventoryViewModel model)
        {
            bool result = false;
            using (POSContext context = new POSContext())
            {
                // ambil dulu dari Database
                var item = context.pos_item_inventory.Find(model.id);
                // menghapus data Mahasiswa dari object context
                context.pos_item_inventory.Remove(item);
                try
                {
                    // commit ke database
                    context.SaveChanges();
                    result = true;
                }
                catch (Exception)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
