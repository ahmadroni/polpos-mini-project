﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using POS.Repo;
using POS.ViewModel;


namespace POS.Web.Controllers
{
    // edit
    public class MstCustomerController : Controller
    {
        // GET: Customer
        public ActionResult Index()
        {
            return View(MstCustomerRepo.Get());
        }

        public ActionResult List()
        {
            return PartialView("_List", MstCustomerRepo.Get());
        }

        // search
        public ActionResult Search(string keySearch)
        {
            return PartialView("_List", MstCustomerRepo.Get(keySearch));
        }

        public ActionResult Add()
        {
            ViewBag.ListProvince = new SelectList(MstProvinceRepo.Get(), "id", "name");
            ViewBag.ListRegion = new SelectList(String.Empty, "", "");
            ViewBag.ListDistrict = new SelectList(String.Empty, "", "");
            return PartialView("_Add");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Add(MstCustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstCustomerRepo.Insert(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Simpan Gagal"
                    });
                }
            }
            ViewBag.ListProvince = new SelectList(MstProvinceRepo.Get(), "id", "name");
            ViewBag.ListRegion = new SelectList(String.Empty, "", "");
            ViewBag.ListDistrict = new SelectList(String.Empty, "", "");
            return PartialView("_Add", model);
        }

        public ActionResult Edit(int id)
        {
            ViewBag.ListProvince = new SelectList(MstProvinceRepo.Get(), "id", "name");
            ViewBag.ListRegion = new SelectList(MstRegionRepo.Get(), "id", "name");
            ViewBag.ListDistrict = new SelectList(MstDistrictRepo.Get(), "id", "name");
            return PartialView("_Edit", MstCustomerRepo .Get(id));
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(MstCustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstCustomerRepo.Update(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Simpan Gagal"
                    });
                }
            }
            ViewBag.ListProvince = new SelectList(MstProvinceRepo.Get(), "id", "name");
            ViewBag.ListRegion = new SelectList(MstRegionRepo.Get(), "id", "name");
            ViewBag.ListDistrict = new SelectList(MstDistrictRepo.Get(), "id", "name");
            return PartialView("_Edit", model);
        }

        public ActionResult Detail(int id)

        {
            MstCustomerViewModel data = MstCustomerRepo.Get(id);
            return PartialView("_Detail", data);
        }

        public JsonResult Delete(int id)
        {
            if (MstCustomerRepo.Delete(id))
            {
                return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new
                {
                    success = false,
                    message = "Simpan Gagal"
                }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(MstCustomerViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (MstCustomerRepo.Delete(model))
                {
                    return Json(new
                    {
                        success = true,
                        message = "Simpan Berhasil"
                    });
                }
                else
                {
                    return Json(new
                    {
                        success = false,
                        message = "Simpan Gagal"
                    });
                }
            }
            return PartialView("_Delete", model);
        }
    }
}