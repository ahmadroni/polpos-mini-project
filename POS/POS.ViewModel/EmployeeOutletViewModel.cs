﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace POS.ViewModel
{
    public class EmployeeOutletViewModel
    {
        
        public int id { get; set; }
        [Display(Name = "Nama Employee")]
        public int emoloyee_id { get; set; }
        [Display(Name = "Nama Employee")]
        public string employeeName { get; set; }
        [Display(Name = "Nama Outlet")]
        public int outlet_id { get; set; }
        [Display(Name = "Nama Outlet")]
        public string outletName { get; set; }
    }
}
